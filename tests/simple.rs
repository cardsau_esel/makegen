use makegen::*;

#[test]
fn simple() {
    let mut makefile = Makefile::from(
        "Makefile",
        "Makefile with two independent targets."
    );

    makefile.add_target(
        Target::from (
            "t1",
            "The first target.",
            false
        )
    );

    makefile.add_target(
        Target::from (
            "t2",
            "The second target.",
            false
        )
    );

    let now = chrono::offset::Local::now().format( "%Y-%m-%d %H:%M:%S" );
    let expected = format!(
        "#\n# Automatically generated on {}.\n# Do _NOT_ edit!\n#\n{}\n",
        now,
        concat!(
            "# Makefile with two independent targets.\n",
            "\n",
            "# The first target.\n",
            "t1:\n",
            "\n",
            "# The second target.\n",
            "t2:\n"
        )
    );

    assert_eq!( makefile.create_source( 0 ), expected );
}

