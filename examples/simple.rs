use makegen::*;

/**
 * Creates a Makefile with phony targets all and clean, with the targets file1, file2 and
 * file3.
 *
 * The dependencies are out/file3 <- out/file2 <- out/file1
 *
 * The output folder 'out' is created as a directory.
 * Each of the targets creates its output files.
 *
 * 'make all' depends on file3
 * 'make clean' removes the output folder.
 */
pub fn main() -> std::io::Result<()> {
    let mut makefile = Makefile::from(
        "Makefile",
        "Simple Makefile to show the functionality of makegen."
    );

    makefile.add_directory( "out", "The output directory." );

    let mut file1 = Target::from(
        "out/file1",
        "The first file created by touching it.",
        false
    );
    file1.add_command( "@touch $@" );
    makefile.add_target( file1 );

    let mut file2 = Target::from(
        "out/file2",
        "The second file is a copy of file1 and depends on its existence.",
        false
    );
    file2.add_dependency( "out/file1" );
    file2.add_command( "@cp $< $@" );
    makefile.add_target( file2 );

    let mut file3 = Target::from(
        "out/file3",
        "The third file is a concatenation of file1 and file2.",
        false
    );
    file3.add_dependency( "out/file1" );
    file3.add_dependency( "out/file2" );
    file3.add_command( "@cat $^ > $@" );
    makefile.add_target( file3 );

    let mut all = Target::from(
        "all",
        "The target all is the first target in the Makefile and depends on out/file3",
        true
    );
    all.add_dependency( "out/file3" );
    makefile.add_first_target( all );

    let mut clean = Target::from(
        "clean",
        "Cleans up all generated files and folders.",
        true
    );
    clean.add_command( "@rm -rf out" );
    makefile.add_target( clean );

    {
        makefile.create()?;
    }

    Ok(())
}

