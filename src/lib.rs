/* The makefile structure */
mod makefile;

/* Make the Target type publicly available at crate root */
pub use crate::makefile::target::Target;

/* Make the Comment type publicly available at crate root */
pub use crate::makefile::comment::Comment;

/* Make the Makefile type publicly available at crate root */
pub use crate::makefile::Makefile;

