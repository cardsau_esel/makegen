/* The makefile target structure */
pub mod comment;

/* The makefile target structure */
pub mod directory;

/* The makefile target structure */
pub mod target;

/* Use the Target implementation */
use crate::makefile::target::Target;

/* Use the Comment implementation */
use crate::makefile::comment::Comment;

/* Use the directory implementation */
use crate::makefile::directory::Directory;

use std::fs::File;
use std::io::prelude::*;

#[derive(Debug)]
pub struct Makefile
{
    name : String,
    comment : Comment,
    phonies : Vec< String >,
    targets : Vec< Target >,
    directories : Vec< Directory >,
}

impl Makefile
{
    pub fn from( name : & str, comment : & str ) -> Makefile
    {
        Makefile
        {
            name        : String::from( name ),
            comment     : Comment::from( comment ),
            phonies     : Vec::new(),
            targets     : Vec::new(),
            directories : Vec::new()
        }
    }

    pub fn add_directory( & mut self, path : & str, comment : & str )
    {
        self.directories.push( Directory::from( path, comment ) );
    }

    pub fn add_first_target( & mut self, target : Target )
    {
        if target.is_phony()
        {
            self.phonies.insert( 0, format!( "{}", target.name ) );
        }

        self.targets.insert( 0, target );
    }

    pub fn add_target( & mut self, target : Target )
    {
        if target.is_phony()
        {
            self.phonies.push( format!( "{}", target.name ) );
        }

        self.targets.push( target );
    }

    pub fn create_source( self, indent : usize ) -> String
    {
        let mut source = Comment::from( & sourcegen::util::header() ).source( 0 );

        source.push_str( & self.comment.source( 0 ) );

        // Start the directory creation with a linebreak for better readability.
        if self.directories.len() > 0
        {
            source.push_str( "\n" );
        }

        for i in & self.directories
        {
            source.push_str( & i.source( indent ) );
        }

        let indent_str = sourcegen::util::indent( 1 );

        /* Create the list of phony targets
         *
         * .PHONY: \
         *     p1 \
         *     p2 \
         *     ...
         *     pn
         */
        if self.phonies.len() > 0
        {
            source.push_str( "\n" );
            source.push_str( ".PHONY:" );

            for i in & self.phonies
            {
                source.push_str( & format!( " \\\n{}{}", indent_str, i ) );
            }
            source.push_str( "\n" );
        }

        /* Create the targets
         *
         * t1: \
         *     d1 \
         *     ...
         *     dn
         * \tc1
         * ...
         * \tcn
         *
         */
        if self.targets.len() > 0
        {
            source.push_str( "\n" );
        }

        for i in & self.targets
        {
            source.push_str( & i.source() );
            source.push_str( "\n" );
        }

        return source;
    }

    pub fn create( self ) -> std::io::Result<()>
    {
        let mut file = File::create( & self.name ) ?;

        let source = self.create_source( 0 );

        file.write( & source.into_bytes() )?;

        return Ok(())
    }
}

//------------------------------------------------------------------------------
#[cfg(test)]
mod tests
{
    use super::*;

    #[test]
    /**
     * Checks the header and comment creation.
     */
    fn header_and_comment()
    {
        let makefile = Makefile::from
        (
            "Makefile",
            "Just for testing.\nWith a second\nand a third line."
        );

        let comment  = "# Just for testing.\n# With a second\n# and a third line.";

        let now      = chrono::offset::Local::now().format( "%Y-%m-%d %H:%M:%S" );
        let expected = format!
        (
            "#\n# Automatically generated on {}.\n# Do _NOT_ edit!\n#\n{}\n",
            now,
            comment
        );

        assert_eq!( makefile.create_source( 0 ), expected );
    }

    #[test]
    /**
     * Checks the directory creation.
     */
    fn directories()
    {
        let mut makefile = Makefile::from
        (
            "Makefile",
            "Creating directories in Makefile."
        );

        makefile.add_directory( "d1", "Needed for something" );
        makefile.add_directory( "d2", "Needed for something else" );

        let now      = chrono::offset::Local::now().format( "%Y-%m-%d %H:%M:%S" );
        let expected = format!
        (
            "#\n# Automatically generated on {}.\n# Do _NOT_ edit!\n#\n{}\n",
            now,
            concat!
            (
                "# Creating directories in Makefile.\n",
                "\n",
                "# Needed for something\n",
                "$(shell mkdir -p d1)",
                "\n",
                "# Needed for something else\n",
                "$(shell mkdir -p d2)"
            )
        );

        assert_eq!( makefile.create_source( 0 ), expected );
    }

    #[test]
    /**
     * Checks the directory creation.
     */
    fn targets()
    {
        let mut makefile = Makefile::from
        (
            "Makefile",
            "Makefile with two independent targets."
        );

        makefile.add_target
        (
            Target::from
            (
                "t1",
                "The first target.",
                false
            )
        );

        makefile.add_target
        (
            Target::from
            (
                "t2",
                "The second target.",
                false
            )
        );

        let now      = chrono::offset::Local::now().format( "%Y-%m-%d %H:%M:%S" );
        let expected = format!
        (
            "#\n# Automatically generated on {}.\n# Do _NOT_ edit!\n#\n{}\n",
            now,
            concat!
            (
                "# Makefile with two independent targets.\n",
                "\n",
                "# The first target.\n",
                "t1:\n",
                "\n",
                "# The second target.\n",
                "t2:\n"
            )
        );

        assert_eq!( makefile.create_source( 0 ), expected );
    }
}

