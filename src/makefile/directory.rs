/* Use the Makefile comment for object descriptions */
use crate::makefile::comment::Comment;

#[derive(Debug)]
pub struct Directory {
    path : String,
    comment : Comment
}

impl Directory {
    pub fn from( path : & str, comment : & str ) -> Directory {
        Directory {
            path : String::from( path ),
            comment : Comment::from( comment )
        }
    }

    pub fn source( & self, indent : usize ) -> String {
        let mut source = self.comment.source( indent );

        let indent_str = sourcegen::util::indent( indent );

        source.push_str( & format!( "{}$(shell mkdir -p {})\n", indent_str, & self.path ) );

        return source;
    }
}

//------------------------------------------------------------------------------
#[cfg(test)]
mod tests {
    use super::*;

    /**
     * Creates a Directory and reads back its name.
     */
    #[test]
    fn path() {
        let d1 : Directory = Directory::from( "d1", "A descriptive text" );

        assert_eq!( d1.path, String::from( "d1" ) );
    }

    /**
     * Creates a Directory and reads back its comment.
     */
    #[test]
    fn comment() {
        let d1 = Directory::from( "d1", "A descriptive text" );

        assert_eq!( d1.comment.source( 0 ), String::from( "# A descriptive text\n" ) );
    }
}

