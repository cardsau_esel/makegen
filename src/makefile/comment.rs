use sourcegen::comment::singleline::SingleLine;

const COMMENT : &'static str = "#";

#[derive(Debug)]
pub struct Comment
{
    inner : SingleLine
}

impl Comment
{
    pub fn from( text : & str ) -> Comment
    {
        Comment { inner : SingleLine::from( & COMMENT, text ) }
    }

    pub fn source( & self, indent : usize ) -> String
    {
        return self.inner.source( indent );
    }
}

//------------------------------------------------------------------------------
#[cfg(test)]
mod tests
{
    use super::*;

    #[test]
    /**
     * Create a simple single line Makefile comment with one level of
     * indentation.
     */
    fn single()
    {
        let c1 = Comment::from( "Single line comment" );

        assert_eq!
        (
            c1.source( 1 ),
            String::from( "    # Single line comment\n" )
        );
    }
}

