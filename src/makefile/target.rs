/* Use the Makefile comment for target descriptions */
use crate::makefile::comment::Comment;

#[derive(Debug)]
pub struct Target {
    pub name : String,
    comment : Comment,
    dependencies : Vec< String >,
    commands : Vec< String >,
    phony : bool
}

impl Target {
    pub fn from( name : & str, comment : & str, phony : bool ) -> Target {
        Target {
            name : String::from( name ),
            comment : Comment::from( comment ),
            dependencies : Vec::new(),
            commands : Vec::new(),
            phony : phony
        }
    }

    pub fn source( & self ) -> String {
        /* Create the <target>: entry */
        let mut source = self.comment.source( 0 );

        source.push_str( & self.name );
        source.push_str( ":" );

        /* Add each dependency into a new line */
        for i in & self.dependencies {
            let indent_str = sourcegen::util::indent( 1 );
            source.push_str( & format!( " \\\n{}{}", indent_str, i )  );
        }

        source.push_str( "\n" );

        /* Add each command into a new line. Commands must start with a tab '\t' */
        for i in & self.commands {
            source.push_str( & format!( "\t{}\n", i )  );
        }

        return source;
    }

    pub fn add_dependency( & mut self, dependency : & str ) {
        self.dependencies.push( String::from( dependency ) );
    }

    pub fn add_command( & mut self, command : & str ) {
        self.commands.push( String::from( command ) );
    }

    pub fn is_phony( & self ) -> bool { self.phony }
}

impl std::fmt::Display for Target {
    fn fmt( & self, f : & mut std::fmt::Formatter< '_ > ) -> std::fmt::Result {
        write!( f, "{}", self.source() )
    }
}

//------------------------------------------------------------------------------
#[cfg(test)]
mod tests {
    use super::*;

    /**
     * Creates a Target and reads back its name.
     */
    #[test]
    fn name() {
        let t1 = Target::from( "t1", "A descriptive text", false );

        assert_eq!( t1.name, String::from( "t1" ) );
    }

    /**
     * Creates a Target, adds multiple dependencies and commands and checks the
     * source code after each operation.
     */
    #[test]
    fn source() {
        let mut t1 = Target::from( "t1", "A descriptive text", false );
        t1.add_dependency( "d1" );
        t1.add_dependency( "d2" );
        t1.add_command( "c1" );

        assert_eq!
        (
            t1.source(),
            concat!
            (
                "# A descriptive text\n",
                "t1: \\\n",
                "    d1 \\\n",
                "    d2\n",
                "\tc1\n"
            )
        );
    }

    /**
     * Checks the phony attribute getter.
     */
    #[test]
    fn is_not_phony() {
        let t1 = Target::from( "t1", "A descriptive text", false );
        assert_eq!( t1.is_phony(), false );
    }

    /**
     * Checks the phony attribute getter.
     */
    #[test]
    fn is_phony() {
        let t2 = Target::from( "t2", "A descriptive text", true );
        assert_eq!( t2.is_phony(), true );
    }
}

